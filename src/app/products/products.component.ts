import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Product } from '../Product.model';
import { Router} from '@angular/router'


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  Products$? : Product[];
  constructor(private dataService: DataService,private router: Router) { }

  ngOnInit() {
    return this.dataService.getProducts().subscribe(data => this.Products$ = data);   
  }

  editProduct(){
    this.router.navigate(['/Product-list'])
  }

}
