import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { ProductsListComponent} from './products-list/products-list.component'
import { CommonModule } from '@angular/common';
const routes: Routes = [
  {path:  'Product', component: ProductsComponent},
  {path:'Product-list', component: ProductsListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
//export const RoutingComponents = [ProductsComponent]
